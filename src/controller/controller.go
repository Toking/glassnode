package controller

import (
	"be-code-challenge-master/src/db"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type Fee struct {
	T string `json:"t"`
	V string `json:"v"`
}

func GetFees(c *gin.Context)  {
	database, err := db.OpenConnection()
	if err != nil {
		panic(err)
	}

	rows, err := database.Query("SELECT * FROM fees")
	if err != nil {
		log.Fatal(err)
	}

	var feelist []Fee
	for rows.Next() {
		var fee Fee
		rows.Scan(&fee.T, &fee.V)
		feelist = append(feelist, fee)
	}

	defer rows.Close()
	defer database.Close()
	c.JSON(http.StatusOK, feelist)
	return
}