package server

import (
	"be-code-challenge-master/src/controller"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type Server struct {
	Engine  *gin.Engine
}

func NewServer() (*Server, error) {
	gin.SetMode(gin.ReleaseMode)
	s := &Server{
		Engine:  gin.Default(),
	}
	return s, nil
}

func (s *Server) StartServer() {
	s.Engine.Use(CORS)
	s.Engine.GET("/fees", controller.GetFees)

	err := s.Engine.Run(":8080")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Server is started")
}

func CORS(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Methods", "*")
	c.Header("Access-Control-Allow-Headers", "*")
	c.Header("Content-Type", "application/json")
	if c.Request.Method != "OPTIONS" {
		c.Next()
	} else {
		c.AbortWithStatus(http.StatusOK)
	}
}
