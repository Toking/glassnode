package main

import (
	"be-code-challenge-master/src/server"
)

func main() {
	s, err := server.NewServer()
	if err != nil {
		panic(err)
	}

	s.StartServer()
}