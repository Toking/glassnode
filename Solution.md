## Documentation

####Solution has 2 main parts:
- `init.sql`, which is called during startup of the Postgres database and constructs a table `fees` containing the hourly ETH fees from the existing tables `contracts` and `transactions`. Fees are converted from wet to eth, 1 eth == 1e18 wei, so eth units were got by dividing wei fees to 10^18
- Golang REST API with 1 GET request, which serves the content of the `fees` table as JSON.

## Project overview
| File | Description |
|------|-------------|
| init.sql | SQL script constructing the table `fees` |
| main.go  | Start of the server |
| controller.go  | Handler for API calls |
| db.go  | Connection to database |
| server.go  | Gin server config |
| Dockerfile | Docker container for REST API |
| docker-compose.yaml | defines the `glassnode_database` and `glassnode_server` containers |

<br />

## Installation

Run the docker compose up command in the home directory, wait until database is started and ready to accept connections, visit `localhost:8080/fees` in your browser.

```
docker-compose up -d --build
curl localhost:8080/fees
```

<br />

## Filtering result

Unix timestamps and corresponding total hourly ETH fees.

```
[
    {
        "t": "1599436800",
        "v": "17.7819378157073"
    },
    {
        "t": "1599440400",
        "v": "25.7961731588859"
    },
    {
        "t": "1599444000",
        "v": "34.821055861441"
    },
    {
        "t": "1599447600",
        "v": "29.8144934244049"
    },
    {
        "t": "1599451200",
        "v": "27.821774201374"
    },
    {
        "t": "1599454800",
        "v": "25.5753115956576"
    },
    {
        "t": "1599458400",
        "v": "33.1387725956814"
    },
    {
        "t": "1599462000",
        "v": "35.6715047484052"
    },
    {
        "t": "1599465600",
        "v": "29.861742077138"
    },
    {
        "t": "1599469200",
        "v": "31.8705283055473"
    },
    {
        "t": "1599472800",
        "v": "29.4927777398214"
    },
    {
        "t": "1599476400",
        "v": "28.4768951832161"
    },
    {
        "t": "1599480000",
        "v": "31.458479835443"
    },
    {
        "t": "1599483600",
        "v": "36.1148814834539"
    },
    {
        "t": "1599487200",
        "v": "39.9905719528013"
    },
    {
        "t": "1599490800",
        "v": "32.3514613660727"
    },
    {
        "t": "1599494400",
        "v": "35.7027698260357"
    },
    {
        "t": "1599498000",
        "v": "28.2253508335762"
    },
    {
        "t": "1599501600",
        "v": "23.6199745344661"
    },
    {
        "t": "1599505200",
        "v": "20.7925556624104"
    },
    {
        "t": "1599508800",
        "v": "20.3247561568389"
    },
    {
        "t": "1599512400",
        "v": "18.6415032090896"
    },
    {
        "t": "1599516000",
        "v": "16.7782403976198"
    },
    {
        "t": "1599519600",
        "v": "17.399949324596"
    }
]
```